(function(){
    var dateTime = new Date();
    var latestYear = dateTime.getFullYear();
    var latestDay = dateTime.getDay();
    var latestDate = dateTime.getDate();
    var yearHtml = "";
    for(let i = -5; i <= 5; i++ ){
        if(i == 0){
            yearHtml += `<option value="${latestYear + i}" selected>${latestYear + i}</option>`
        }
        else{
            yearHtml += `<option value="${latestYear + i}">${latestYear + i}</option>`
        }
    }
    document.getElementById("years").innerHTML = yearHtml;

    var latestMonth = dateTime.getMonth();
    document.getElementById("months").options[latestMonth].selected = 'selected';

    buildDateHtml(latestDay, latestDate, latestMonth, latestYear);
 
})();


function onClickPrevBtn(){

   var monthIndex = document.getElementById("months").value;
   if(monthIndex == 0){
      monthIndex = 12;
   }
   document.getElementById("months").options[parseInt(monthIndex) - 1].selected = 'selected';

   var year = document.getElementById("years").value;
   var totalYearOption = document.getElementById("years");

   if(monthIndex == 12){
    for(let i = 0; i < totalYearOption.length; i++) {

        if (totalYearOption[i].value == (parseInt(year) - 1)) {
            document.getElementById("years").options[i].selected = 'selected';
        }
      }
   }

   if(monthIndex == 12){
       year = parseInt(year) - 1;
    }
   var FirstDay = new Date(year, (parseInt(monthIndex) - 1), 1);
   FirstDay = FirstDay.getDay();
 
   buildDateHtml(FirstDay, 1, (parseInt(monthIndex) - 1), year)
}


function onClickNextBtn(){

    var monthIndex = document.getElementById("months").value;
    if(monthIndex == 11){
       monthIndex = -1;
    }
    document.getElementById("months").options[parseInt(monthIndex) + 1].selected = 'selected';
 
    var year = document.getElementById("years").value;
    var totalYearOption = document.getElementById("years");
 
    if(monthIndex == -1){
     for(let i = 0; i < totalYearOption.length; i++) {
 
         if (totalYearOption[i].value == (parseInt(year) + 1)) {
             document.getElementById("years").options[i].selected = 'selected';
         }
       }
    }

    if(monthIndex == -1){
        year = parseInt(year) + 1;
     }
    var FirstDay = new Date(year, (parseInt(monthIndex) + 1), 1);
    FirstDay = FirstDay.getDay();
  
    buildDateHtml(FirstDay, 1, (parseInt(monthIndex) + 1), year)
 
 }


 function buildDateHtml(latestDay, latestDate, latestMonth, latestYear){

    var dayOfFirstDate = latestDay;
    var datesHtml = "";
    var totalDays;
    for(let i = 1 ; i < latestDate; i++){
        if(dayOfFirstDate == -1){
            dayOfFirstDate = 6;
        }
        dayOfFirstDate = dayOfFirstDate - 1;
    }

    datesHtml = `<tr> 
                    <td>Sun</td>       
                    <td>Mon</td>
                    <td>Tue</td>
                    <td>Wed</td>
                    <td>Thu</td>
                    <td>Fri</td>
                    <td>Sat</td>

                 </tr>`

    datesHtml += `<tr>`;
    for(let i = 0; i < dayOfFirstDate; i++){
        datesHtml += `<td></td>`
    }

    if(latestMonth <= 6){
        if(latestMonth % 2 == 0){
            totalDays = 31
         }
         else if(latestMonth == 1){
              //Determing if February (28,or 29)  
            
                 if ( (latestYear % 100!=0) && (latestYear % 4==0) || (latestYear % 400==0)){
                     totalDays = 29;
                 }else{
                     totalDays = 28;
                 }
     
         }
         else{
             totalDays = 30;
         }
         
    }
    else{
        if(latestMonth % 2 == 0){
            totalDays = 30
         }
         else{
            totalDays = 31
         }
    }

   var k = 0;
    if(dayOfFirstDate == 6){
        datesHtml += `<td>1</td>`;
        datesHtml += `</tr><tr>`;
       

        for( let i = 2; i <= totalDays; i++){


            if((dayOfFirstDate + i) == 8){
                datesHtml += `</tr><tr>`;
            }
           else if((dayOfFirstDate + i) > 7){
              ++k;
              if((k%7 == 0) && k != totalDays){
                datesHtml += `</tr><tr>`;
              } 
           }
            datesHtml += `<td>${i}</td>`;
           
            if(k == totalDays){
                datesHtml += `</tr>`;
            }
        }
    }
    else{
        for( let i = 1; i <= totalDays; i++){

            if((dayOfFirstDate + i) == 8){
                datesHtml += `</tr><tr>`;
            }
           else if((dayOfFirstDate + i) > 7){
              ++k;
              if((k%7 == 0) && k != totalDays){
                datesHtml += `</tr><tr>`;
              } 
           }
            datesHtml += `<td>${i}</td>`;
           
            if(k == totalDays){
                datesHtml += `</tr>`;
            }
    
        }
    }
  

    document.getElementById("montday").innerHTML = datesHtml;
 }

 function onChangeDropDown(){
     var month = document.getElementById("months").value;
     var year = document.getElementById("years").value;

     var FirstDay = new Date(parseInt(year), parseInt(month), 1);
     FirstDay = FirstDay.getDay();

     buildDateHtml(FirstDay, 1, parseInt(month), parseInt(year))
 }
 var classFlag = 0;

 function onBlurInputBox(){
    if(classFlag == 0){
        document.getElementById("tableRow").setAttribute("style", "display:block;");
        classFlag = 1;
    }
    else{
        document.getElementById("tableRow").setAttribute("style", "display:none;");
        classFlag = 0;
    }
 }