var calendar={
    months : ["January","February","March","April","May","June","July","August","September","October","November","December"],
    weeks : ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
    minDate :"",
    maxDate :"",
    root :"",
    c :100,
    minusCentury: -100,
    //days:["31","28","31","30","31","30","31","31","30","31","30","31"],
    //leapdays:["31","29","31","30","31","30","31","31","30","31","30","31"],
    previousTd : "",
    prevNextMonth : 0,
    selectedDate : "",
    toShow : "",
    weekStart :""
}
var datePicker = {
    init :function(){
        calendar.root = this;
        this.bindCalendar();
        this.setCurrentDate();
    },
    bindCalendar:function(){
        var calendarHtml = "";
        var today = new Date();
        var divCalendar = $("#calendar");
        //calendarHtml+="<div>&nbsp;</div>";
        calendarHtml +="<table width=\"100%\" class=\"table \" style=\"margin-bottom: -30px;\">";
        calendarHtml +="<tr class=\"bg-dark text-white\">";
        calendarHtml +=" <td width=\"0%\"><a  onclick=\"datePicker.prevNextMonth(-1)\"><i class=\"fa fa-angle-double-left\" aria-hidden=\"true\"></i> Prev</a></td>"+
                        "<td width=\"50%\" id=\"selectedDate\" style=\"text-align:center;\">Selected Date</td>"+
                        "<td width=\"0%\"class=\"float-right\" colspan=\"2\"><a  onclick=\"datePicker.prevNextMonth(1)\">Next <i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i></a></td>";
                        //"<td width=\"10%\">&nbsp;</td></tr>";
        calendarHtml +="<tr class=\"bg-secondary text-white\" style=\"text-align:center;\">";
        calendarHtml+="<td style=\"padding: 5px;\" colspan=\"2\">";
        calendarHtml+=calendar.root.bindMonths();
        calendarHtml+="</td>";
        calendarHtml+="<td style=\"padding: 5px;\" colspan=\"2\">";
        calendarHtml+=calendar.root.bindYears();
        calendarHtml+="</td>"+
                        "</tr>"+
                        "<tr>"+
                            "<td colspan=\"3\"></td>"+
                        "</tr>"+
                        "</table>";
        calendarHtml +="<div class=\"space\"></div>";
        calendarHtml +="<table width=\"100%\">"+
        "<tr style=\"text-align:center\" class=\"bg-danger text-white\">";
        calendarHtml +=calendar.root.bindWeek();
        calendarHtml += "</tr><tr>";
        calendarHtml +="<td colspan=\"7\">";
        calendarHtml+="<table class=\"table table-bordered bg-info text-white\" id=\"bindDates\">";
        calendarHtml+=calendar.root.getDates(today.getDay(),today.getDate(),today.getMonth(),today.getFullYear());
        calendarHtml+="</table></td></tr>";
        $(divCalendar).html(calendarHtml);
        $(divCalendar).fadeIn();
    },
    bindMonths :function(){
        var monthHtml ="";
        var count = 0
        monthHtml+="<select class=\"form-control form-control-sm\" id=\"drpMonth\" onchange=\"datePicker.changeMonthYear()\">";
        calendar.months.forEach(element => {
            monthHtml+="<option value="+count+">"+element+"</option>";
            count ++;
        });
        monthHtml+="</select>"
        return monthHtml;
    },
    bindYears:function(){
        var yearsHtml = "";
        var minYear = calendar.minusCentury+new Date().getFullYear();
        var maxYear = calendar.c+new Date().getFullYear();
        yearsHtml +="<select class=\"form-control form-control-sm\" id=\"drpYear\" onchange=\"datePicker.changeMonthYear()\">";
        for(var i=minYear;i<=maxYear;i++){
            yearsHtml +="<option value='"+i+"'>"+i+"</option>";
        }
        yearsHtml += "</select>";
        return yearsHtml;
    },
    bindWeek:function(){
        var weekDaysHtml = "";
        weekDaysHtml +="<tr style=\"text-align:center\" class=\"bg-danger text-white\">";
        if(calendar.weekStart!=""){
            var index = calendar.weeks.indexOf(calendar.weekStart);
            calendar.weeks = calendar.weeks.concat(calendar.weeks.splice(0,index));
        }
        calendar.weeks.forEach(element=>{
            weekDaysHtml+="<td style=\"text-align:center;\">"+element+"</td>";
        });
        weekDaysHtml+="</tr>";
        return weekDaysHtml;
    },
    getDates:function(latestDay,latestDate,latestMonth,latestYear){
        var dayOfFirstDate = latestDay;
    var datesHtml = "";
    var totalDays;
    for(let i = 1 ; i < latestDate; i++){
        if(dayOfFirstDate == -1){
            dayOfFirstDate = 6;
        }
        dayOfFirstDate = dayOfFirstDate - 1;
    }
    datesHtml += `<tr>`;
    for(let i = 0; i < dayOfFirstDate; i++){
        datesHtml += `<td></td>`
    }

    if(latestMonth <= 6){
        if(latestMonth % 2 == 0){
            totalDays = 31
         }
         else if(latestMonth == 1){
              //Determing if February (28,or 29)  
            
                 if ( (latestYear % 100!=0) && (latestYear % 4==0) || (latestYear % 400==0)){
                     totalDays = 29;
                 }else{
                     totalDays = 28;
                 }
     
         }
         else{
             totalDays = 30;
         }
         
    }
    else{
        if(latestMonth % 2 == 0){
            totalDays = 30
         }
         else{
            totalDays = 31
         }
    }

   var k = 0;
    if(dayOfFirstDate == 6){
        datesHtml += `<td>1</td>`;
        datesHtml += `</tr><tr>`;
       

        for( let i = 2; i <= totalDays; i++){


            if((dayOfFirstDate + i) == 8){
                datesHtml += `</tr><tr>`;
            }
           else if((dayOfFirstDate + i) > 7){
              ++k;
              if((k%7 == 0) && k != totalDays){
                datesHtml += `</tr><tr>`;
              } 
           }
           if(i== new Date().getDate() && latestMonth == new Date().getMonth()){
            datesHtml += `<td class='today' onclick='datePicker.selectedDate(this)'>`+i+`</td>`;
           }
           else if(i==parseInt(calendar.selectedDate.split('/')[0])){
            datesHtml += `<td class='selected' onclick='datePicker.selectedDate(this)'>`+i+`</td>`;
           }
           else{
            datesHtml += `<td onclick='datePicker.selectedDate(this)'>`+i+`</td>`;
           }
           
            if(k == totalDays){
                datesHtml += `</tr>`;
            }
        }
    }
    else{
        for( let i = 1; i <= totalDays; i++){

            if((dayOfFirstDate + i) == 8){
                datesHtml += `</tr><tr>`;
            }
           else if((dayOfFirstDate + i) > 7){
              ++k;
              if((k%7 == 0) && k != totalDays){
                datesHtml += `</tr><tr>`;
              } 
           }
           if(i== new Date().getDate() && latestMonth == new Date().getMonth()){
            datesHtml += `<td class='today' onclick='datePicker.selectedDate(this)'>`+i+`</td>`;
           }
           else if(i==parseInt(calendar.selectedDate.split('/')[0])){
            datesHtml += `<td class='selected' onclick='datePicker.selectedDate(this)'>`+i+`</td>`;
           }
           else{
            datesHtml += `<td onclick='datePicker.selectedDate(this)'>`+i+`</td>`;
           }
           
            if(k == totalDays){
                datesHtml += `</tr>`;
            }
    
        }
    }
        return datesHtml;
    },
    setCurrentDate :function(){
        var drpMonth = $("#drpMonth");
        var drpYear = $("#drpYear");
        var today = new Date();
        $(drpMonth).val(today.getMonth());
        $(drpYear).val(today.getFullYear());
        var month = parseInt(today.getMonth())+1;
        var selectedDate = today.getDate()+'/'+month+'/'+today.getFullYear();
        if(calendar.selectedDate==""){
            calendar.selectedDate = selectedDate;
        }
        var tdSelecteDate = $("#selectedDate");
        $(tdSelecteDate).html(calendar.selectedDate);
        //console.log(today.getDate());
    },
    selectedDate:function(td){
        td.className = "selected"
        var date = td.textContent;
        if(calendar.previousTd==""){
            calendar.previousTd = td ;
        }
        else{
            if(calendar.previousTd.textContent == new Date().getDate()){
                calendar.previousTd.className = "today";
            }else{
            calendar.previousTd.className = "";}
            calendar.previousTd = td;
        }
        var month = parseInt($("#drpMonth").val())+1;
        var year = $("#drpYear").val();
        var selectedDate = date+"/"+month+"/"+year;
        var tdSelecteDate = $("#selectedDate");
        $(tdSelecteDate).text(selectedDate);
        calendar.selectedDate = selectedDate;
        if(calendar.toShow.tagName.toLowerCase()=="input"){
            calendar.toShow.value = selectedDate;
        }
        else{
            calendar.toShow.innerHTML = selectedDate;
        }
        var divCalendar = $("#calendar");
        $(divCalendar).fadeOut();
        //alert(td.text);
        //console.log(td);
        //alert(td.textContent);
    },
    changeMonthYear :function(){
        var month = $("#drpMonth").val();
        var year = $("#drpYear").val();
        var latestDay = new Date(year,month,1).getDay();
        var tableDate = $('#bindDates');
        //tableDate.innerHTML = calendar.root.bindDays(parseInt(month),parseInt(year));
        $(tableDate).html(calendar.root.getDates(latestDay,1,parseInt(month),parseInt(year)));
    },
    prevNextMonth : function(value){
        var month = $("#drpMonth").val();
        var year = $("#drpYear").val();
        var tableDate = $('#bindDates');

        calendar.prevNextMonth = parseInt(month)+value;
        var latestDay = '';
        if(calendar.prevNextMonth == 12){
            calendar.prevNextMonth = 0;
            $("#drpMonth").val(0);
            year= parseInt(year)+1;
            $("#drpYear").val(year);
            
        }
        else{
            if(calendar.prevNextMonth  == -1){
                $("#drpMonth").val(11);
                year=parseInt(year)-1;
                $("#drpYear").val(year);
                
            }
            else{
                $("#drpMonth").val(calendar.prevNextMonth);
                
            }
        }
        latestDay = new Date(year,calendar.prevNextMonth,1).getDay();
        tableDate.innerHTML = calendar.root.getDates(latestDay,1,calendar.prevNextMonth,parseInt(year));
    }
}